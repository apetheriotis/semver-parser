package com.angelos.petheriotis.versiontooling

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import org.scalatest.prop.TableDrivenPropertyChecks

import scala.util.Try

class SemVerSpec extends AnyFlatSpec with TableDrivenPropertyChecks {

  private val isGreaterThan = Table(
    ("this", "other", "isGreaterThan"),
    (SemVer("1.2.3"), SemVer("1.2.2"), true),
    (SemVer("1.2.3"), SemVer("1.2.3"), false),
    (SemVer("1.2.3"), SemVer("0.9.0"), true),
    (SemVer("1.0.0"), SemVer("1.0.1"), false),
    (SemVer("1.0.0"), SemVer("0.0.1"), true),
    (SemVer("1.2.3"), SemVer("1.3.3"), false),
    (SemVer("0.9.99"), SemVer("1.9.0"), false)
  )

  private val isLessThan = Table(
    ("this", "other", "isLessThan"),
    (SemVer("1.2.3"), SemVer("1.2.2"), false),
    (SemVer("1.2.3"), SemVer("1.2.3"), false),
    (SemVer("1.2.3"), SemVer("0.9.0"), false),
    (SemVer("1.2.3"), SemVer("1.3.3"), true),
    (SemVer("0.9.99"), SemVer("1.9.0"), true),
    (SemVer("1.0.0"), SemVer("1.0.1"), true)
  )

  private val isGreaterThanOrEqualTo = Table(
    ("this", "other", "isGreater"),
    (SemVer("1.2.3"), SemVer("1.2.2"), true),
    (SemVer("1.2.3"), SemVer("1.2.3"), true),
    (SemVer("1.2.3"), SemVer("0.9.0"), true),
    (SemVer("1.2.3"), SemVer("1.3.3"), false),
    (SemVer("0.9.99"), SemVer("1.9.0"), false),
    (SemVer("0.9.99"), SemVer("0.9.99"), true),
    (SemVer("0.0.1"), SemVer("0.0.1"), true)
  )

  private val isLessThanOrEqualTo = Table(
    ("this", "other", "isGreater"),
    (SemVer("1.2.3"), SemVer("1.2.2"), false),
    (SemVer("1.2.3"), SemVer("1.2.3"), true),
    (SemVer("1.2.3"), SemVer("0.9.0"), false),
    (SemVer("1.2.3"), SemVer("1.3.3"), true),
    (SemVer("0.9.99"), SemVer("1.9.0"), true),
    (SemVer("0.9.99"), SemVer("0.9.99"), true),
    (SemVer("0.0.1"), SemVer("0.0.1"), true)
  )

  private val isEqualTo = Table(
    ("this", "other", "isGreaterOrEqual"),
    (SemVer("1.2.3"), SemVer("1.2.2"), false),
    (SemVer("1.2.3"), SemVer("1.2.4"), false),
    (SemVer("1.2.3"), SemVer("1.2.3"), true),
    (SemVer("0.0.3"), SemVer("0.0.3"), true),
  )

  it should "properly evaluate is greater than operator" in {
    forAll(isGreaterThan) { (left: Try[SemVer], right: Try[SemVer], result: Boolean) =>
      left.get > right.get shouldBe result
    }
  }

  it should "properly evaluate is greater than or equal to operator" in {
    forAll(isGreaterThanOrEqualTo) { (left: Try[SemVer], right: Try[SemVer], result: Boolean) =>
      left.get >= right.get shouldBe result
    }
  }

  it should "properly evaluate is equal to operator" in {
    forAll(isEqualTo) { (left: Try[SemVer], right: Try[SemVer], result: Boolean) =>
      left.get == right.get shouldBe result
    }
  }

  it should "properly evaluate is less than  operator" in {
    forAll(isLessThan) { (left: Try[SemVer], right: Try[SemVer], result: Boolean) =>
      left.get < right.get shouldBe result
    }
  }

  it should "properly evaluate is less than or equal to operator" in {
    forAll(isLessThanOrEqualTo) { (left: Try[SemVer], right: Try[SemVer], result: Boolean) =>
      left.get =< right.get shouldBe result
    }
  }
}
