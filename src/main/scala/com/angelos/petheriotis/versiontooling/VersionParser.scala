package com.angelos.petheriotis.versiontooling

import scala.util.parsing.combinator.RegexParsers

object VersionParser extends RegexParsers {

  def versionExpression: Parser[Version] = {
    number ~ dot.? ~ number.? ~ dot.? ~ number.? ^^ {
      case major ~ Some(_) ~ Some(minor) ~ Some(_) ~ Some(patch) => SemVer(major, minor, patch) // X.Y.Z
      case major ~ Some(_) ~ Some(minor) ~ None ~ None           => SemVer(major, minor, 0) // X.Y
      case major ~ None ~ None ~ None ~ None                     => SemVer(major, 0, 0) // X
      case _ ~ Some(_) ~ None ~ Some(_) ~ Some(_)                => InvalidVersion("minor version has to be defined when patch is already defined") // X..Z
      case _ ~ Some(_) ~ _ ~ Some(_) ~ None                      => InvalidVersion("invalid version. Remove trailing dots") // X.Y.
      case _ ~ Some(_) ~ None ~ None ~ None                      => InvalidVersion("invalid version. Remove trailing dots") // X.
    }
  }

  def dot: Parser[Any] = "."

  def number: Parser[Int] =
    """(0|[1-9]\d*)""".r ^^ {
      _.toInt
    }

  def parse(input: String): Either[Throwable, SemVer] = {
    super.parse(versionExpression, input) match {
      case Success(version: SemVer, _)         => Right(version)
      case Success(version: InvalidVersion, _) => Left(new Throwable(version.error))
      case Failure(error, _)                   => Left(new Throwable(error))
      case Error(error, _)                     => Left(new Throwable(error))
    }
  }

}
