scalaVersion := "2.13.1"
name := "semver-parsers"
organization := "com.angelos.petheriotis.versiontooling"
version := "1.0"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2",
  "org.scalatest"          %% "scalatest"                % "3.1.0" % "test"
)
