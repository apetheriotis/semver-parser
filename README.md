## Definition

#### Task
  Create an abstraction to represent version strings (see detail below) such that they
  can be compared to one another.
  Once you have implemented this, you should add some test cases to demonstrate
  the functionality. Bonus points for covering the edge cases!
  
  You should not use any third party libraries. If the language you implement this in
  already has an abstraction for representing version strings then you should not use
  it in the implementation (although you may use it in the tests if desired).
  You may implement this is Python, Java, Kotlin or Scala. It should be
  straighforward to take the code and run the tests (e.g. if Python, it could be a single
  executable module).
  
#### Version strings
  Versions strings in this context are semver-like (https://semver.org/) in that they
  have a numerical major, minor and patch section X.Y.Z
  Where X is the major version, Y is the minor version, Z is the patch version.
  The values sections (X, Y, Z) must all be positive integers.
  You can ignore suffixes to the version string such as &quot;alpha&quot; in 10.1.3-alpha
  
  The lower version numbers are optional and if missing are equivalent to 0. E.G. 3 == 3.0 == 3.0.0
  
#### Comparison
  The required comparisons are:
   - Greater than 
   - Greater than or equal to 
   - Less than
   - Less than or equal to 
   - Equal to
   
   
## How to run
in order to run the tests `cd` to the root of this project and simply run `sbt test`. The output should be similar to:

```
[info] SemVerSpec:
[info] - should properly evaluate is greater than operator
[info] - should properly evaluate is greater than or equal to operator
[info] - should properly evaluate is equal to operator
[info] - should properly evaluate is less than  operator
[info] - should properly evaluate is less than or equal to operator
[info] VersionParserSpec:
[info] - should parse valid version
[info] - should parse valid version with leading zeros
[info] - should parse valid version with trailing zeros
[info] - should parse valid version with missing patch version
[info] - should parse valid version with missing minor version
[info] - should ignore pre-release metadata
[info] - should ignore build metadata
[info] - should ignore pre-release and build metadata
[info] - should not parse empty version
[info] Run completed in 423 milliseconds.
[info] Total number of tests run: 14
[info] Suites: completed 2, aborted 0
[info] Tests: succeeded 14, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
```