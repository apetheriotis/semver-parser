package com.angelos.petheriotis.versiontooling

import scala.util.Try

/**
  * High level representation of a version
  */
trait Version

case class InvalidVersion(error: String) extends Version

/**
  * Semantic Version wrapper that offers basic comparison operations
  */
case class SemVer(major: Int, minor: Int, patch: Int) extends Version {

  /**
    * Less than comparison
    */
  def <(other: SemVer): Boolean = !(this > other) && !(this == other)

  /**
    * Less than or equal to comparison
    */
  def =<(other: SemVer): Boolean = !(this > other) || this == other

  /**
    * Equal to comparison
    */
  def ==(other: SemVer): Boolean =
    if (this.major == other.major && this.minor == other.minor && this.patch == other.patch) true
    else false

  /**
    * Greater than or equal to comparison
    */
  def >=(other: SemVer): Boolean = this > other || this == other

  /**
    * Greater than comparison
    */
  def >(other: SemVer): Boolean = {
    if (this.major > other.major) {
      true
    } else if (this.major < other.major) {
      false
    } else if (this.minor > other.minor) {
      true
    } else if (this.minor < other.minor) {
      false
    } else if (this.patch > other.patch)
      true
    else {
      false
    }
  }

}

object SemVer {

  /**
    * convenient method to create a SemVer from a string
    */
  def apply(input: String): Try[SemVer] = {
    VersionParser.parse(input).toTry
  }
}
