package com.angelos.petheriotis.versiontooling

import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class VersionParserSpec extends AnyFlatSpec with EitherValues {

  it should "parse valid version" in {
    VersionParser.parse("1.2.3").right.value shouldBe SemVer(1, 2, 3)
  }

  it should "parse valid version with leading zeros" in {
    VersionParser.parse("0.0.3").right.value shouldBe SemVer(0, 0, 3)
  }

  it should "parse valid version with trailing zeros" in {
    VersionParser.parse("3.0.0").right.value shouldBe SemVer(3, 0, 0)
  }

  it should "parse valid version with missing patch version" in {
    VersionParser.parse("1.2").right.value shouldBe SemVer(1, 2, 0)
  }

  it should "parse valid version with missing minor version" in {
    VersionParser.parse("1").right.value shouldBe SemVer(1, 0, 0)
  }

  it should "ignore pre-release metadata" in {
    VersionParser.parse("1.2.3-alpha").right.value shouldBe SemVer(1, 2, 3)
  }

  it should "ignore build metadata" in {
    VersionParser.parse("1.2.3+sha256").right.value shouldBe SemVer(1, 2, 3)
  }

  it should "ignore pre-release and build metadata" in {
    VersionParser.parse("1.2.3-alpha+sha256").right.value shouldBe SemVer(1, 2, 3)
  }

  it should "not parse empty version" in {
    VersionParser
      .parse("")
      .left
      .value
      .getMessage shouldBe """string matching regex '(0|[1-9]\d*)' expected but end of source found"""
  }

  it should "not parse the version when only minor is missing" in {
    VersionParser
      .parse("1..1")
      .left
      .value
      .getMessage shouldBe """minor version has to be defined when patch is already defined"""
  }

  it should "not parse the version when minor and patch numbers are missing" in {
    VersionParser
      .parse("1..")
      .left
      .value
      .getMessage shouldBe """invalid version. Remove trailing dots"""
  }

  it should "not parse the version when patch is missing after an empty dot" in {
    VersionParser
      .parse("1.2.")
      .left
      .value
      .getMessage shouldBe """invalid version. Remove trailing dots"""
  }

  it should "not parse the version when there is a trailing dot after the major" in {
    VersionParser
      .parse("1.")
      .left
      .value
      .getMessage shouldBe """invalid version. Remove trailing dots"""
  }
}
